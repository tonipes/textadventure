## README ##
3D adventure game rendered to ASCII art.
Runnable version can be found [here](https://www.dropbox.com/s/54lqlw0ot1ij59y/Awesome_Adventure.zip?dl=0)

## Story ##
The year is 20XX. A deadline is fast approaching, and it is your task to hand in a document on time. But the previous night's events has caused the pages to scatter! Gather them all up, stay awake, and hand in your work before 06:00 AM.

## Credits ##
 * Toni Pesola - https://bitbucket.org/diTony/
 * Juuso Toikka - https://bitbucket.org/jtoikka/

## Notes ##
This repository is only for archiving purposes.